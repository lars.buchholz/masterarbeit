from rembg import remove
from PIL import Image, ImageOps
import easygui as eg
import random

total_images = 500 #Anzahl der zu bearbeitenden Bilder

#Pfad zum Ordner der zu bearbeitenden Bilder
path_to_base_images = "/home/lars/Desktop/bcad_files/Fahrrad Generiert/"

#Pfad zu den Hintergrundbildern
path_to_backgrounds = "/home/lars/Desktop/bcad_files/Realistic Backgrounds/"

#Pfad zu einem Zwischenordner um rotierte Bilder zu speichern
temp_rotate_folder = "/home/lars/Desktop/bcad_files/Fahrrad Generiert (Rotated)/"

#Pfad zu einem Zwischenordner um gespiegelte Bilder zu speichern
temp_mirrored_folder = "/home/lars/Desktop/bcad_files/Fahrrad Generiert (Mirrored)/"

#Pfad zu einem Zwischenordner um Bilder ohne Hintergrund zu speichern
temp_nobg_folder = "/home/lars/Desktop/bcad_files/Fahrrad Generiert (No BG)/"

#Pfad zum Ordner für die komplett bearbeiteten Bilder
path_to_finished_images = "/home/lars/Desktop/bcad_files/Fahrrad Generiert 300x300/"

#Funktion um weißen Hintergrund aus Bildern zu entfernen
def removeWhiteBackground(source_path, target_path, number):
    img = Image.open(source_path +str(number) +".png")
    img = img.convert("RGBA")
  
    datas = img.getdata()
  
    newData = []
  
    for item in datas:
        if (item[0] == 255 and item[1] == 255 and item[2] == 255) or (item[0] == 0 and item[1] == 0 and item[2] == 0):
            newData.append((255, 255, 255, 0))
        else:
            newData.append(item)
            
    img.putdata(newData)
    img.save(target_path +str(number) +".png", "PNG")

#Funktion um Bild zufällig um 0-5 Grad zu drehen
def rotateImage(source_path, target_path, number):
    img = Image.open(source_path +str(number) +".png")
    
    rotation = random.randint(0,5)
    
    new_img = img.rotate(rotation)
    
    new_img.save(target_path +str(number) +".png", "PNG")

#Funktion um Bild zufällig horizontal zu spiegeln
def mirrorImage(source_path, target_path, number):
    img = Image.open(source_path +str(number) +".png")
    
    mirror = random.randint(0,1)
    
    if mirror>0:
        new_img = ImageOps.mirror(img)
    else:
        new_img = img
    
    new_img.save(target_path +str(number) +".png", "PNG")

#Funktion um Bilder auf 300x300 Pixel zu setzen und einen von 15 zufälligen Hintergrundbildern hinzuzufügen
def addBackgroundandResize(source_path, target_path, path_to_backgrounds, number):
    background_number = random.randint(1,15)
    
    background = Image.open(path_to_backgrounds +str(background_number) +".png")
    foreground = Image.open(source_path+str(number+1) +".png")

    resized_background = background.resize((300,300))
    resized_foreground = foreground.resize((300,300))

    resized_background.paste(resized_foreground, (0,0), resized_foreground)

    resized_background.save(target_path +str(image+1) +".png")

#Anwenden der Funktionen auf die Bilder
for image in range(total_images):
    rotateImage(path_to_base_images,temp_rotate_folder,image+1)
    mirrorImage(temp_rotate_folder,temp_mirrored_folder,image+1)
    removeWhiteBackground(temp_mirrored_folder,temp_nobg_folder,image+1)
    addBackgroundandResize(temp_nobg_folder,path_to_finished_images,image+1)
    print("Finished image: " +str(image+1))
    
