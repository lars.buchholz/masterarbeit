import random
import math

desired_bcad_file_amount = 500 #Anzahl der gewünschten neuen bcad-Dateien
path_to_base_bcad = "/home/lars/Desktop/Base.bcad" #Pfad zur Base.bcad Datei
target_folder = "/home/lars/Desktop/bcad_files/" #Pfad zum Ordner in dem die neuen bcad-Dateien gespeichert werden sollen

for file_number in range(desired_bcad_file_amount):

#Folgend werden die 13 Faktoren welche in der Base.bcad Datei manipuliert werden, jeweils in realitischen Rahmen randomisiert

    #Faktor 1 Sattelhöhe
    saddle_height_base = "saddle_height_number"
    saddle_height_new = str(random.randint(630,810))

    #Faktor 2 Sitzstangenlänge
    seat_tube_length_base = "seat_tube_length_number"
    seat_tube_length_new = str(random.randint(390,530))

    #Faktor 3 Tretlagerabfall
    bb_drop_base = "bb_drop_number"
    bb_drop_new = str(random.randint(50,90))
    
    #Faktor 4 Kettenstrebenlänge
    chain_stay_length_base = "chain_stay_length_number"
    chain_stay_length_new = str(random.randint(325,485))
    
    #Faktor 5 Sitzstangenverlängerung
    seat_tube_extension_base = "seat_tube_extension_number"
    seat_tube_extension_new = str(random.randint(40,60))

    #Faktor 6 Reach
    front_center_distance_base = "front_center_distance_number"
    front_center_distance_new = str(reach = random.randint(310,470))
    
    #Faktor 7 Stack
    stack_base = "stack_number"
    stack_new = str(random.randint(435,695))

    #Faktor 8 Steuerrohrlänge
    head_tube_length_base = "head_tube_length_number"
    head_tube_length_new = str(random.randint(120,180))

    #Faktor 9 Obere Steuerrohrverlängerung
    head_tube_upper_extension_base = "head_tube_upper_extension_number"
    head_tube_upper_extension_new = str(random.randint(20,40))

    #Faktor 10 Untere Steuerrohrverlängerung
    head_tube_lower_extension_base = "head_tube_lower_extension_number"
    head_tube_lower_extension_new = str(random.randint(40,60))
    
    #Faktor 11 Rahmenfarbe
    color = random.randint(0,7)
    colors = [-65536, -3158064, -1118482, -26368, -256, -16735488, -16777053, -65281]
    colors_text = ["red", "black", "grey", "orange", "yellow", "green", "blue", "fuchsia"]
    color_base = "FIRST_color_sRGB_number"
    color_new = str(colors[color])

    #Faktor 12 Reifendurchmesser
    wheel_diameter_base = "wheel_diameter_number"
    wheel_diameter_new = str(random.randint(500,700))
    
    #Setzt Reifendicke auf fest 25 cm
    bsd_base = "bsd_number"
    bsd_new = str(wheel_metric-25)
    
    #Faktor 13 Kettenschutzstil (50% keine, 50% 1 von 3 Stilen)
    chainguard_base = "show_chainguard"
    chainguard_rand = random.randint(0,1)
    if chainguard_rand>0:
        chainguard_new = "true"
    else:
        chainguard_new = "false"
    chainguardstyle_base = "chainguardstyle"
    chainguardstyle_new = str(random.randint(1,3))
    
    #Lesen Base.bcad Datei und einfügen der randomisierten Faktoren
    with open(path_to_base_bcad, 'r') as file:
        data = file.read()
        data = data.replace(saddle_height_base, saddle_height_new)
        data = data.replace(seat_tube_length_base, seat_tube_length_new)
        data = data.replace(bb_drop_base, bb_drop_new)
        data = data.replace(chain_stay_length_base, chain_stay_length_new)
        data = data.replace(seat_tube_extension_base, seat_tube_extension_new)
        data = data.replace(front_center_distance_base, front_center_distance_new)
        data = data.replace(head_tube_length_base, head_tube_length_new)
        data = data.replace(head_tube_upper_extension_base, head_tube_upper_extension_new)
        data = data.replace(head_tube_lower_extension_base, head_tube_lower_extension_new)
        data = data.replace(color_base, color_new)
        data = data.replace(wheel_diameter_base, wheel_diameter_new)
        data = data.replace(bsd_base, bsd_new)
        data = data.replace(background_base, background_new)
        data = data.replace(stack_base, stack_new)
        data = data.replace(chainguard_base, chainguard_new)
        data = data.replace(chainguardstyle_base, chainguardstyle_new)
      
    #Speichern der neuen bcad-Datei
    new_filename = "target_folder" +str(file_number+1) +".bcad"
    with open(new_filename, 'x') as file2:
        file2.write(data)