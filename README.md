# Masterarbeit

## Übersicht der Inhalte

### Python Scripte-Ordner
- bcad_creator.py
- random_edits.py 

In diesem Odner sind die in der Arbeit beschriebenen Python-Scripte zu finden.


### Modelle-Ordner
- Modell__1.zip
- Modell__2.zip
- Modell__3.zip
- Modell__4.zip
- Modell__5.zip
- Modell__6.zip

In diesem Ordner sind die fertig trainierten Modelle aus der Arbeit zu finden. Sie wurden im TensorFlow Lite Format exportiert, welches optimiert ist um auf mobilen Endgeräten genutzt zu werden (https://www.tensorflow.org/lite/guide)

Übersicht der Modelle

![alt text](https://gitlab.ub.uni-bielefeld.de/lars.buchholz/masterarbeit/-/raw/main/modelluebersicht.png)

### Datensätze

Unter folgenden Link sind alle annotierten realen und synthetischen Bilde zu finden, sowie die in der Arbeit genutzten Datensätze zum trainieren, validieren und testen.

Datensätze: https://uni-bielefeld.sciebo.de/s/68MAQSo1vP0Boom

Übersicht der Datensätze

![alt text](https://gitlab.ub.uni-bielefeld.de/lars.buchholz/masterarbeit/-/raw/main/DatensatzUebersicht.png)
